Latex Templates
================
By: Jonathan Shahen


TexStudio Templates
-------------------
 * This folder contain single page templates
 * The templates contained within this folder should be copied to `%AppData%\texstudio\templates\user`
 * Make sure to copy the `.tex` as well as the `.json` files
 * These projects should be compiled with `pdflatex`

Full Templates
--------------
 * This folder contains latex projects that have many files
 * Each project is contained within a folder and the whole folder should be copied in order to work
 * These projects should be compiled with `pdflatex` unless otherwise stated
 
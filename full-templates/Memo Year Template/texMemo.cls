% This document class provides a simple memo for LaTeX users.
% It is based on article.cls and inherits most of the functionality
% that class.
% 
% Author: Rob Oakes, Copyright 2010.  Released under the LGPL, version 3.
% A copy of the LGPL can be found at http://www.gnu.org/licenses/lgpl.html
% Edited: Jonathan Shahen, 2017

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{texMemo}[2010/07/31 - Simple Memo Class, Including Logo]
\RequirePackage{palatino}

% Load the Base Class
\LoadClassWithOptions{article}

% Begin Requirements
\RequirePackage{ifthen}

% Specialized memo Commands (To, From, Subject, Logo, Letterhead Address)

\def\@memoto{\relax}
\newcommand{\memoto}[1]{\gdef\@memoto{#1}}

\def\@memofrom{\relax}
\newcommand{\memofrom}[1]{\gdef\@memofrom{#1}}

\def\@memosubject{\relax}
\newcommand{\memosubject}[1]{\gdef\@memosubject{#1}}

\def\@memokeywords{\relax}
\newcommand{\memokeywords}[1]{\gdef\@memokeywords{#1}}

\def\@memodate{\relax}
\newcommand{\memodate}[1]{\gdef\@memodate{#1}}

\def\@memologo{\relax}
\newcommand{\logo}[1]{\gdef\@memologo{\protect #1}}

\def\@letterheadaddress{\relax}
\newcommand{\lhaddress}[1]{\gdef\@letterheadaddress{#1}}

% Custom Document Formatting
\newcommand\decorativeline[1][1pt]{
	\par\noindent%
	\rule[0.5ex]{\linewidth}{#1}\par
}

% Set the Paper Size and margins
\RequirePackage{geometry}
\geometry{margin=1.0in}

% Create the Letterhead and To/From Block

\renewcommand{\maketitle}{\makememotitle}
\newcommand\makememotitle{
	\ifthenelse{\equal{\@memologo}{\relax}}{}
	{ % Create With Logo
	\begin{minipage}[t]{1\columnwidth}%
		\begin{flushright}
			\vspace{-0.6in}
			\@memologo
			\vspace{0.5in}
		\par\end{flushright}%
	\end{minipage}
	}
	
	% To, From, Subject Block
    {\begin{center}
    \Large\bf
    M\textsc{emorandum}
    \end{center}}
   	\begin{tabular}{rl}
   		\ifthenelse{\equal{\@memoto}{\relax}}{}{       \textbf{To:} & \@memoto \\}
   		\ifthenelse{\equal{\@memofrom}{\relax}}{}{     \textbf{From:} & \@memofrom \\}
   		\ifthenelse{\equal{\@memosubject}{\relax}}{}{  \textbf{Subject:} & \@memosubject \\}
   		\ifthenelse{\equal{\@memodate}{\relax}}{}{     \textbf{Date:} & \@memodate \\}
   		\ifthenelse{\equal{\@memokeywords}{\relax}}{}{ \textbf{\footnotesize Keywords:} & {\footnotesize \@memokeywords} \\}
   	\end{tabular}
%   % OLD METHOD
%	\begin{description}
%		\ifthenelse{\equal{\@memoto}{\relax}}{}{\item [{To:}] \@memoto}
%		\ifthenelse{\equal{\@memofrom}{\relax}}{}{\item [{From:}] \@memofrom}
%		\ifthenelse{\equal{\@memosubject}{\relax}}{}{\item [{Subject:}] \@memosubject}
%		\ifthenelse{\equal{\@memokeywords}{\relax}}{}{\item [{Keywords:}] \@memokeywords}
%		\ifthenelse{\equal{\@memodate}{\relax}}{}{\item [{Date:}] \@memodate}
%	\end{description}
	\decorativeline\bigskip{}
}
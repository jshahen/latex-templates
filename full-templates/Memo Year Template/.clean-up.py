import os
import glob

def main():
    os.chdir(os.path.dirname(__file__))

    bad_extensions = ['.log', '.aux', '.gz']

    for ext in bad_extensions:
        for f in glob.glob('*'+ext):
            os.remove(f)

if __name__ == '__main__':
    main()